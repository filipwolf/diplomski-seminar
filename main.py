from sklearn.cluster import SpectralClustering
import numpy as np
from itertools import chain, combinations


def powerset(iterable):
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s) + 1))


if __name__ == "__main__":

    with open("./32-34.txt", 'r') as graph_data:

        # A = []
        biggest = 0

        read = graph_data.readlines()

        for i in read:
            a = int(i.split('\t')[0])
            b = int(i.split('\t')[1])

            if a > biggest:
                biggest = a
            if b > biggest:
                biggest = b

        D = np.zeros(biggest + 1)
        B = np.diag(np.zeros(biggest + 1))
        n_clusters_range = 13

        for i in read:
            a = int(i.split('\t')[0])
            b = int(i.split('\t')[1])

            D[a] += 1
            D[b] += 1
            B[a][b] = 1
            B[b][a] = 1
            # c = []
            # for j in range(biggest):
            #     if a == j:
            #         c.append(-1)
            #     elif b == j:
            #         c.append(1)
            #     else:
            #         c.append(0)
            # A.append(np.array(c))

        # A = np.array(A)
        # print("bla")
        # L = np.dot(np.transpose(A), A)
        # print(L)
        # L = np.subtract(np.diag(D), B)
        # print(L)
        print(B)
        # print(D)

        final = []

        for n_clusters in range(3, n_clusters_range):

            print("n_clusters: " + str(n_clusters))

            clustering = SpectralClustering(n_clusters=n_clusters, eigen_solver='amg', affinity='precomputed',
                                            assign_labels='discretize').fit(B)
            # print(clustering.labels_)
            # print()
            # for i in range(len(clustering.labels_)):
            #     if clustering.labels_[i] == 0:
            #         print(i)

            results = list(powerset([i for i in range(n_clusters)]))
            results = results[:-1]

            rank = []
            writeList = []

            for i in range(len(results)):
                C = np.zeros(biggest + 1)
                for j in range(len(read)):
                    a = int(read[j].split('\t')[0])
                    b = int(read[j].split('\t')[1])
                    if clustering.labels_[a] not in results[i]:
                        C[b] += 1
                    if clustering.labels_[b] not in results[i]:
                        C[a] += 1

                # print(C)
                C = np.delete(C, np.where(C == 0.))
                rank.append([results[i], np.mean(C), len(C)])
                # print(str(results[i]) + ": " + str(avgRank) + ", size:" + str(len(C)))

            rank = sorted(rank, key=lambda comb: comb[1])
            # print(rank)
            for i in rank:
                if i[1] < 1.97:
                    final.append(i)
                    writeList.append(i)
            print(final)
            print()

            for i in range(len(writeList)):
                f = open("file_" + str(writeList[i][0]) + "_" + str(i) + ".txt", "w")
                for j in range(len(read)):
                    a = int(read[j].split('\t')[0])
                    b = int(read[j].split('\t')[1])
                    if clustering.labels_[a] not in writeList[i][0] and clustering.labels_[b] not in writeList[i][0]:
                        f.write(read[j])
                f.close()

        final = sorted(final, key=lambda comb: comb[2], reverse=True)
        print(final)
